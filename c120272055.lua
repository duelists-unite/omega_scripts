local m=120272055
local cm=_G["c"..m]
cm.name="死地高扬"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY+CATEGORY_DRAW)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCost(cm.cost)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
end
--Activate
function cm.costfilter(c)
	return (c:IsLocation(LOCATION_HAND) or (c:IsFaceup() and c:IsRace(RACE_ZOMBIE)))
		and c:IsAbleToGraveAsCost()
end
function cm.exfilter(c)
	return c:IsRace(RACE_ZOMBIE)
end
function cm.check(g)
	return g:GetClassCount(Card.GetLocation)==g:GetCount()
end
cm.cost=RD.CostSendHandOrFieldSubToGrave(cm.costfilter,cm.check,2,2)
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(nil,tp,0,LOCATION_ONFIELD,1,nil) end
	local g=Duel.GetMatchingGroup(nil,tp,0,LOCATION_ONFIELD,nil)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,1,0,0)
end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	RD.SelectAndDoAction(HINTMSG_DESTROY,nil,tp,0,LOCATION_ONFIELD,1,1,nil,function(g)
		if Duel.Destroy(g,REASON_EFFECT)~=0
			and Duel.IsExistingMatchingCard(cm.exfilter,tp,LOCATION_GRAVE,0,4,nil) then
			RD.CanDraw(aux.Stringid(m,1),tp,1,true)
		end
	end)
end