local m=120109042
local list={120263009,120263006}
local cm=_G["c"..m]
cm.name="元素英雄 泥球侠"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
	--Only Fusion Summon
	RD.OnlyFusionSummon(c)
end