local m=120151025
local cm=_G["c"..m]
cm.name="宿灵椅·消酸痛式"
function cm.initial_effect(c)
	--Discard Deck
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetCategory(CATEGORY_DECKDES+CATEGORY_TOHAND+CATEGORY_GRAVE_ACTION+CATEGORY_RECOVER)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(cm.cost)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.operation)
	c:RegisterEffect(e1)
end
--Discard Deck
function cm.costfilter(c)
	return c:IsAttack(0) and c:IsAbleToGraveAsCost()
end
function cm.exfilter(c)
	return c:IsLevelBelow(8) and c:IsAttribute(ATTRIBUTE_DARK)
end
function cm.thfilter(c)
	return c:IsLevelBelow(8) and c:IsAttribute(ATTRIBUTE_DARK) and c:IsAbleToHand()
end
cm.cost=RD.CostSendHandToGrave(cm.costfilter,1,1)
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDiscardDeck(tp,2) end
	Duel.SetOperationInfo(0,CATEGORY_DECKDES,nil,0,tp,2)
end
function cm.operation(e,tp,eg,ep,ev,re,r,rp)
	if RD.SendDeckTopToGraveAndExists(tp,2,cm.exfilter,1,nil) then
		RD.SelectAndDoAction(HINTMSG_ATOHAND,aux.NecroValleyFilter(cm.thfilter),tp,LOCATION_GRAVE,0,1,1,nil,function(g)
			if RD.SendToHandAndExists(g,e,tp,REASON_EFFECT) then
				Duel.Recover(1-tp,g:GetFirst():GetAttack(),REASON_EFFECT)
			end
		end)
	end
end