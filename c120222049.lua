local m=120222049
local cm=_G["c"..m]
cm.name="泡沫王国"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	c:RegisterEffect(e1)
	--Atk Up
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_FZONE)
	e2:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
	e2:SetTarget(cm.uptg)
	e2:SetValue(500)
	c:RegisterEffect(e2)
end
--Atk Up
function cm.uptg(e,c)
	return c:IsFaceup() and c:IsType(TYPE_NORMAL) and c:IsRace(RACE_AQUA)
end