local m=120257060
local cm=_G["c"..m]
cm.name="魔法除去"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
end
--Activate
function cm.filter(c)
	return c:IsFacedown() or c:IsType(TYPE_SPELL)
end
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.filter,tp,0,LOCATION_SZONE,1,nil) end
end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	RD.SelectAndDoAction(aux.Stringid(m,1),cm.filter,tp,0,LOCATION_SZONE,1,1,nil,function(g)
		local tc=g:GetFirst()
		if tc:IsFacedown() then
			Duel.ConfirmCards(tp,tc)
		end
		if tc:IsType(TYPE_SPELL) then
			Duel.Destroy(tc,REASON_EFFECT)
		end
	end)
end