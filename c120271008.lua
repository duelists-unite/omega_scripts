local m=120271008
local cm=_G["c"..m]
cm.name="古代的机械士兵"
function cm.initial_effect(c)
	-- Cannot Activate
	local e1=RD.ContinuousAttackNotChainTrap(c)
	--Continuous Effect
	RD.AddContinuousEffect(c,e1)
end